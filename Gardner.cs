﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using MathNet.Numerics;
using System.Collections.Generic;

namespace SDRSharp.ScytaleC
{
    public class Gardner
    {
        private readonly double T;
        private double symbol2xTimer;
        private int samplesAgo;
        private int isOnPoint;
        private Complex32 currentSample;
        private Complex32 sampleThisOn;
        private Complex32 error;
        private Complex32 sampleLastOn;
        private Complex32 sampleOff;
        private readonly Complex32 aggression;

        public bool Locked { get; private set; }

        public Gardner(double sampleRate, double symbolRate)
        {
            T = sampleRate / symbolRate;
            symbol2xTimer = 0.0;
            samplesAgo = 0;
            isOnPoint = 1;
            sampleLastOn = 0f;
            aggression = 0.15f;
            Locked = false;
        }

        public List<Complex32> ProcessSamples(Complex32[] samples)
        {
            List<Complex32> output = new List<Complex32>();
            Complex32 signalPower = new Complex32(0f, 0f);

            for (int i = 0; i < samples.Length; i++)
            {
                currentSample = samples[i];

                symbol2xTimer += 2;

                // simple way to avoid double sampling due to timing jitter
                samplesAgo++;

                if (symbol2xTimer >= T && samplesAgo > T / 4)
                {
                    symbol2xTimer -= T;
                    samplesAgo = 0;
                    isOnPoint = 1 - isOnPoint;

                    // on time here
                    if (isOnPoint == 1)
                    {
                        sampleThisOn = currentSample;

                        // calculate carrier timing error
                        error = (sampleThisOn - sampleLastOn) * sampleOff;

                        // steer symbol timing
                        symbol2xTimer = symbol2xTimer + (aggression * error).Real;

                        // save on symbol
                        output.Add(sampleThisOn);

                        //update power measurement
                        signalPower += sampleThisOn.Square();

                        // save this on time for next on time
                        sampleLastOn = sampleThisOn;
                    }

                    // we are off time here
                    else
                    {
                        sampleOff = currentSample;
                    }
                }
            }

            //compute the lock as the average of sum of squared inphase branch compared to a magicnumber, 7 up is the norm
            Locked = signalPower.Real / output.Count > 8;

            return output;
        }
    }
}
