﻿namespace SDRSharp.ScytaleC
{
    partial class DecoderPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (sampleProcessor != null)
            {
                sampleProcessor.Dispose();
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.displayTimer = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblLostBuffers = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.LinkLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtFrameNumber = new System.Windows.Forms.TextBox();
            this.txtLost = new System.Windows.Forms.TextBox();
            this.lblOffset = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblDecoders = new System.Windows.Forms.Label();
            this.chkAutoTracking = new System.Windows.Forms.CheckBox();
            this.txtOffset = new System.Windows.Forms.TextBox();
            this.txtFrequency = new System.Windows.Forms.TextBox();
            this.udUDPPort = new System.Windows.Forms.NumericUpDown();
            this.txtUDPAddress = new System.Windows.Forms.TextBox();
            this.lblTxMSym = new System.Windows.Forms.Label();
            this.txtTx = new System.Windows.Forms.TextBox();
            this.lblDebug = new System.Windows.Forms.Label();
            this.txtSYM = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkFrameRecorder = new System.Windows.Forms.CheckBox();
            this.btnOpenUI = new System.Windows.Forms.Button();
            this.lblLocked = new System.Windows.Forms.Label();
            this.cbEnabled = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.constellation1 = new SDRSharp.ScytaleC.Constellation();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udUDPPort)).BeginInit();
            this.groupBox2.SuspendLayout(); 
            this.SuspendLayout();
            // 
            // displayTimer
            // 
            this.displayTimer.Enabled = true;
            this.displayTimer.Interval = 60;
            this.displayTimer.Tick += new System.EventHandler(this.DisplayTimer_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblLostBuffers);
            this.groupBox1.Controls.Add(this.lblVersion);
            this.groupBox1.Controls.Add(this.tableLayoutPanel2);
            this.groupBox1.Controls.Add(this.lblLocked);
            this.groupBox1.Controls.Add(this.cbEnabled);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(238, 245);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // lblLostBuffers
            // 
            this.lblLostBuffers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLostBuffers.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblLostBuffers.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblLostBuffers.Location = new System.Drawing.Point(8, 226);
            this.lblLostBuffers.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLostBuffers.Name = "lblLostBuffers";
            this.lblLostBuffers.Size = new System.Drawing.Size(84, 12);
            this.lblLostBuffers.TabIndex = 11;
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblVersion.ForeColor = System.Drawing.Color.DeepPink;
            this.lblVersion.LinkColor = System.Drawing.Color.DeepPink;
            this.lblVersion.Location = new System.Drawing.Point(99, 226);
            this.lblVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(128, 12);
            this.lblVersion.TabIndex = 74;
            this.lblVersion.TabStop = true;
            this.lblVersion.Text = "vx.x.x.x";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblVersion.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LblVersion_LinkClicked);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.Controls.Add(this.txtFrameNumber, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.txtLost, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.lblOffset, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblDecoders, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.chkAutoTracking, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtOffset, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtFrequency, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.udUDPPort, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtUDPAddress, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblTxMSym, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtTx, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.lblDebug, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtSYM, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.chkFrameRecorder, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.btnOpenUI, 0, 6);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(230, 223);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // txtFrameNumber
            // 
            this.txtFrameNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFrameNumber.Location = new System.Drawing.Point(96, 148);
            this.txtFrameNumber.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtFrameNumber.Name = "txtFrameNumber";
            this.txtFrameNumber.ReadOnly = true;
            this.txtFrameNumber.Size = new System.Drawing.Size(61, 23);
            this.txtFrameNumber.TabIndex = 7;
            // 
            // txtLost
            // 
            this.txtLost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLost.Location = new System.Drawing.Point(165, 119);
            this.txtLost.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtLost.Name = "txtLost";
            this.txtLost.ReadOnly = true;
            this.txtLost.Size = new System.Drawing.Size(61, 23);
            this.txtLost.TabIndex = 6;
            // 
            // lblOffset
            // 
            this.lblOffset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOffset.AutoSize = true;
            this.lblOffset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblOffset.Location = new System.Drawing.Point(4, 65);
            this.lblOffset.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOffset.Name = "lblOffset";
            this.lblOffset.Size = new System.Drawing.Size(84, 15);
            this.lblOffset.TabIndex = 18;
            this.lblOffset.Text = "Offset && CF";
            this.lblOffset.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Location = new System.Drawing.Point(4, 7);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 15);
            this.label4.TabIndex = 16;
            this.label4.Text = "UDP Address";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDecoders
            // 
            this.lblDecoders.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDecoders.AutoSize = true;
            this.lblDecoders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblDecoders.Location = new System.Drawing.Point(4, 36);
            this.lblDecoders.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDecoders.Name = "lblDecoders";
            this.lblDecoders.Size = new System.Drawing.Size(84, 15);
            this.lblDecoders.TabIndex = 16;
            this.lblDecoders.Text = "UDP Port";
            this.lblDecoders.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkAutoTracking
            // 
            this.chkAutoTracking.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.chkAutoTracking.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.chkAutoTracking, 2);
            this.chkAutoTracking.Location = new System.Drawing.Point(96, 90);
            this.chkAutoTracking.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkAutoTracking.Name = "chkAutoTracking";
            this.chkAutoTracking.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.chkAutoTracking.Size = new System.Drawing.Size(130, 22);
            this.chkAutoTracking.TabIndex = 4;
            this.chkAutoTracking.Text = "Auto Tracking";
            this.chkAutoTracking.UseVisualStyleBackColor = true;
            this.chkAutoTracking.CheckedChanged += new System.EventHandler(this.ChkAutoTracking_CheckedChanged);
            // 
            // txtOffset
            // 
            this.txtOffset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOffset.Location = new System.Drawing.Point(96, 61);
            this.txtOffset.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtOffset.Name = "txtOffset";
            this.txtOffset.ReadOnly = true;
            this.txtOffset.Size = new System.Drawing.Size(61, 23);
            this.txtOffset.TabIndex = 2;
            // 
            // txtFrequency
            // 
            this.txtFrequency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFrequency.Location = new System.Drawing.Point(165, 61);
            this.txtFrequency.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtFrequency.Name = "txtFrequency";
            this.txtFrequency.ReadOnly = true;
            this.txtFrequency.Size = new System.Drawing.Size(61, 23);
            this.txtFrequency.TabIndex = 3;
            // 
            // udUDPPort
            // 
            this.udUDPPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.udUDPPort, 2);
            this.udUDPPort.Location = new System.Drawing.Point(96, 32);
            this.udUDPPort.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.udUDPPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.udUDPPort.Name = "udUDPPort";
            this.udUDPPort.Size = new System.Drawing.Size(130, 23);
            this.udUDPPort.TabIndex = 1;
            // 
            // txtUDPAddress
            // 
            this.txtUDPAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.txtUDPAddress, 2);
            this.txtUDPAddress.Location = new System.Drawing.Point(96, 3);
            this.txtUDPAddress.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtUDPAddress.Name = "txtUDPAddress";
            this.txtUDPAddress.Size = new System.Drawing.Size(130, 23);
            this.txtUDPAddress.TabIndex = 0;
            this.txtUDPAddress.Text = "localhost";
            // 
            // lblTxMSym
            // 
            this.lblTxMSym.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTxMSym.AutoSize = true;
            this.lblTxMSym.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTxMSym.Location = new System.Drawing.Point(4, 123);
            this.lblTxMSym.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTxMSym.Name = "lblTxMSym";
            this.lblTxMSym.Size = new System.Drawing.Size(84, 15);
            this.lblTxMSym.TabIndex = 24;
            this.lblTxMSym.Text = "Tx && EstLost";
            this.lblTxMSym.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTxMSym.Click += new System.EventHandler(this.LblTxLost_Click);
            // 
            // txtTx
            // 
            this.txtTx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTx.Location = new System.Drawing.Point(96, 119);
            this.txtTx.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtTx.Name = "txtTx";
            this.txtTx.ReadOnly = true;
            this.txtTx.Size = new System.Drawing.Size(61, 23);
            this.txtTx.TabIndex = 5;
            // 
            // lblDebug
            // 
            this.lblDebug.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDebug.AutoSize = true;
            this.lblDebug.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lblDebug.Location = new System.Drawing.Point(4, 94);
            this.lblDebug.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDebug.Name = "lblDebug";
            this.lblDebug.Size = new System.Drawing.Size(84, 15);
            this.lblDebug.TabIndex = 11;
            this.lblDebug.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDebug.Visible = false;
            // 
            // txtSYM
            // 
            this.txtSYM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSYM.Location = new System.Drawing.Point(165, 148);
            this.txtSYM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtSYM.Name = "txtSYM";
            this.txtSYM.ReadOnly = true;
            this.txtSYM.Size = new System.Drawing.Size(61, 23);
            this.txtSYM.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(4, 152);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 15);
            this.label1.TabIndex = 27;
            this.label1.Text = "FRM && MSys";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkFrameRecorder
            // 
            this.chkFrameRecorder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.chkFrameRecorder.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.chkFrameRecorder, 2);
            this.chkFrameRecorder.Location = new System.Drawing.Point(96, 177);
            this.chkFrameRecorder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkFrameRecorder.Name = "chkFrameRecorder";
            this.chkFrameRecorder.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.chkFrameRecorder.Size = new System.Drawing.Size(130, 22);
            this.chkFrameRecorder.TabIndex = 10;
            this.chkFrameRecorder.Text = "Frame Recorder";
            this.chkFrameRecorder.UseVisualStyleBackColor = true;
            this.chkFrameRecorder.CheckedChanged += new System.EventHandler(this.ChkFrameRecorder_CheckedChanged);
            // 
            // btnOpenUI
            // 
            this.btnOpenUI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOpenUI.FlatAppearance.BorderColor = System.Drawing.Color.LimeGreen;
            this.btnOpenUI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenUI.Location = new System.Drawing.Point(3, 177);
            this.btnOpenUI.Name = "btnOpenUI";
            this.btnOpenUI.Size = new System.Drawing.Size(86, 23);
            this.btnOpenUI.TabIndex = 9;
            this.btnOpenUI.Text = "Quick UI";
            this.btnOpenUI.UseVisualStyleBackColor = false;
            this.btnOpenUI.Click += new System.EventHandler(this.BtnOpenUI_Click);
            // 
            // lblLocked
            // 
            this.lblLocked.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLocked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblLocked.ForeColor = System.Drawing.Color.Green;
            this.lblLocked.Location = new System.Drawing.Point(176, 1);
            this.lblLocked.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLocked.Name = "lblLocked";
            this.lblLocked.Size = new System.Drawing.Size(50, 20);
            this.lblLocked.TabIndex = 17;
            this.lblLocked.Text = "Locked   ";
            this.lblLocked.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbEnabled
            // 
            this.cbEnabled.AutoSize = true;
            this.cbEnabled.Location = new System.Drawing.Point(10, 0);
            this.cbEnabled.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbEnabled.Name = "cbEnabled";
            this.cbEnabled.Size = new System.Drawing.Size(68, 19);
            this.cbEnabled.TabIndex = 3;
            this.cbEnabled.Text = "Enabled";
            this.cbEnabled.UseVisualStyleBackColor = true;
            this.cbEnabled.CheckedChanged += new System.EventHandler(this.CbEnabled_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.constellation1); 
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 245);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Size = new System.Drawing.Size(238, 272);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Constellation";
            //
            // constellation1
            // 
            this.constellation1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.constellation1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.constellation1.Location = new System.Drawing.Point(3, 16);
            this.constellation1.Name = "constellation1";
            this.constellation1.Size = new System.Drawing.Size(198, 187);
            this.constellation1.TabIndex = 0;
            // 
            // DecoderPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "DecoderPanel";
            this.Size = new System.Drawing.Size(238, 517);
            this.Load += new System.EventHandler(this.DecoderPanel_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udUDPPort)).EndInit();

            this.groupBox2.ResumeLayout(false); 
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer displayTimer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbEnabled;
        private System.Windows.Forms.Label lblLocked;
        private System.Windows.Forms.Label lblOffset;
        private System.Windows.Forms.Label lblDecoders;
        private System.Windows.Forms.NumericUpDown udUDPPort;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUDPAddress;
        private System.Windows.Forms.TextBox txtOffset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.CheckBox chkAutoTracking;
        private System.Windows.Forms.TextBox txtFrequency;
        private System.Windows.Forms.Label lblTxMSym;
        private System.Windows.Forms.TextBox txtTx;
        private System.Windows.Forms.TextBox txtSYM;
        private System.Windows.Forms.Label lblLostBuffers;
        private System.Windows.Forms.Label lblDebug;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFrameNumber;
        private System.Windows.Forms.TextBox txtLost;
        private System.Windows.Forms.GroupBox groupBox2;
        private Constellation constellation1;
        private System.Windows.Forms.CheckBox chkFrameRecorder;
        private System.Windows.Forms.LinkLabel lblVersion;
        private System.Windows.Forms.Button btnOpenUI;
    }
}
