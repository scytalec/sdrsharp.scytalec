# README #

Downloads and installation:  
https://bitbucket.org/scytalec/scytalec/downloads/


### Credits ###

This code would not have been possible without the help of:

* Otti, who came with the idea of this plugin,

* Vasili, who kindly advised, send and allowed me to use and modify his TcpServer plugin,

* Jonti, who kindly advised, send and allowed me to use and modify his Gardner and RRC code, 

* Youssef, who kindly took me through some parts of the SDR# code,

* The book: Best, Roland. Costas Loops Theory, Design, and Simulation. Springer International Publishing, 2018., 

* Richard and Terry, who kindly tested and reported the initial issues,

* John, testing and ideas,

* The Community, ever welcoming and cheerful.


### Licensing ###

GNU General Public License 3, microp11 2021
 
Scytale-C is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Scytale-C is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

See http://www.gnu.org/licenses/.
