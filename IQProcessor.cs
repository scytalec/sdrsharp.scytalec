﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using SDRSharp.Radio;

namespace SDRSharp.ScytaleC
{
    public unsafe class IQProcessor : IIQProcessor
    {
        public delegate void IQReadyDelegate(Complex* buffer, double samplerate, int length);
        public event IQReadyDelegate IQReady;

        public double SampleRate { get; set; }

        public bool Enabled { get; set; }

        public void Process(Complex* buffer, int length)
        {
            IQReady?.Invoke(buffer, SampleRate, length);
        }
    }
}