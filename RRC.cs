﻿/*
 * Paul Maxan, microp11 2021
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using MathNet.Filtering.FIR;
using MathNet.Numerics;
using System;

namespace SDRSharp.ScytaleC
{
    public class Rrc
    {
        private OnlineFirFilter rrcFirI;
        private OnlineFirFilter rrcFirQ;

        private readonly double[] points;
        private readonly double alpha;
        private readonly int firstSize;

        public Rrc(double sampleRate, double symbolRate)
        {
            alpha = 1;
            firstSize = 144;

            if ((firstSize % 2) == 0)
            {
                firstSize++;
            }

            points = new double[firstSize];

            double T = sampleRate / symbolRate;
            double phi;
            for (int i = 0; i < firstSize; i++)
            {
                if (i == ((firstSize - 1) / 2))
                {
                    points[i] = (4.0 * alpha + Math.PI - Math.PI * alpha) / (Math.PI * Math.Sqrt(T));
                }
                else
                {
                    phi = (((double)i) - ((double)(firstSize - 1)) / 2.0);
                    if (Math.Abs(1.0 - Math.Pow(4.0 * alpha * phi / T, 2)) < 0.0000000001)
                    {
                        points[i] = (alpha * ((Math.PI - 2.0) * Math.Cos(Math.PI / (4.0 * alpha)) + (Math.PI + 2.0) * Math.Sin(Math.PI / (4.0 * alpha))) / (Math.PI * Math.Sqrt(2.0 * T)));
                    }
                    else
                    {
                        points[i] = (4.0 * alpha / (Math.PI * Math.Sqrt(T)) * (Math.Cos((1.0 + alpha) * Math.PI * phi / T) + T / (4.0 * alpha * phi) * Math.Sin((1.0 - alpha) * Math.PI * phi / T)) / (1.0 - Math.Pow(4.0 * alpha * phi / T, 2)));
                    }
                }
            }

            rrcFirI = new OnlineFirFilter(points);
            rrcFirQ = new OnlineFirFilter(points);
        }

        public double[] ProcessSamples(double[] samples)
        {
            return rrcFirI.ProcessSamples(samples);
        }

        public Complex32[] ProcessSamples(Complex32[] samples)
        {
            int len = samples.Length;
            double[] inputReal = new double[len];
            double[] inputImaginary = new double[len];
            double[] outputReal;
            double[] outputImaginary;
            Complex32[] output = new Complex32[len];
            for (int i = 0; i < len; i++)
            {
                inputReal[i] = samples[i].Real;
                inputImaginary[i] = samples[i].Imaginary;
            }
            outputReal = rrcFirI.ProcessSamples(inputReal);
            outputImaginary = rrcFirQ.ProcessSamples(inputImaginary);

            for (int i = 0; i < len; i++)
            {
                output[i] = new Complex32((float)outputReal[i], (float)outputImaginary[i]);
            }

            return output;
        }

        public void Reset()
        {
            rrcFirI.Reset();
            rrcFirQ.Reset();
        }
    }
}
