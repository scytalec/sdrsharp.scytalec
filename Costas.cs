﻿/*
 * Paul Maxan, microp11 2021
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using MathNet.Filtering.FIR;
using MathNet.Numerics;
using System;

namespace SDRSharp.ScytaleC
{
    /// <summary>
    /// Costas classic carrier recovery
    /// </summary>
    class Costas
    {
        private double frequency;
        private readonly double alpha;
        private readonly int samplerate;
        private readonly double beta;
        private double omega;
        private double phase;
        private double error;

        // LPF1, LPF2
        // The literature suggestes that the square-shaped LPF filter should have a
        // bandwidth of no less than half the symbol rate. This will remove the most
        // noise possible without reducing the amplitude of the desired signal at
        // its sampling instants.
        private readonly double[] blpf12 = { 0.5000, 0.5000 };
        private OnlineFirFilter lpf1;
        private OnlineFirFilter lpf2;

        // The literature indicates that the loop filter should have a
        // response far outside the LPF1/LPF2.The fastest achievable settle
        // time is one in which the VCO has a gain 8x that of the LPF1/LPF2 pole
        // frequency. For LPF3, a factor of six times K or 12 times the LPF1/LPF2
        // pole is a better choice.
        private readonly double[] blpf3 = { 0.0100, 0.7000 };
        private OnlineFirFilter lpf3;

        public double InstantFrequency { get; private set; }

        public Costas()
        {
            frequency = OperationConsts.CenterFrequency;
            samplerate = OperationConsts.SampleRateInt;
            alpha = 0.005;
            phase = 0.0;
            beta = alpha * alpha / 4;

            // carrier
            omega = OperationConsts.TwoPi * frequency / samplerate;

            // I and Q LPFs
            lpf1 = new OnlineFirFilter(blpf12);
            lpf2 = new OnlineFirFilter(blpf12);

            // loop LPF
            lpf3 = new OnlineFirFilter(blpf3);
        }

        public void Reset()
        {
            lpf1.Reset();
            lpf2.Reset();
            lpf3.Reset();
            omega = OperationConsts.TwoPi * OperationConsts.CenterFrequency / OperationConsts.SampleRate;
            phase = 0.0;
            error = 0.0;
        }

        public void SetCenterFrequency(double centerFrequency)
        {
            omega = OperationConsts.TwoPi * centerFrequency / samplerate;
        }

        public Complex32[] ProcessSamples(Complex32[] samples)
        {
            int len = samples.Length;
            Complex32[] result = new Complex32[len];
            Complex32[] samples_normalized = new Complex32[len];

            double I;
            double Q;
            double magnitudeRe = 0.0;
            double meanMagnitudeRe;
            double magnitudeIm = 0.0;
            double meanMagnitudeIm;

            // sample normalization
            for (int i = 0; i < len; i++)
            {
                magnitudeRe += Math.Abs(samples[i].Real);
                magnitudeIm += Math.Abs(samples[i].Imaginary);

            }
            meanMagnitudeRe = magnitudeRe / len;
            meanMagnitudeIm = magnitudeIm / len;

            for (int i = 0; i < len; i++)
            {
                samples_normalized[i] = new Complex32((float)(samples[i].Real / meanMagnitudeRe), (float)(samples[i].Imaginary / meanMagnitudeIm));
            }

            for (int i = 0; i < len; i++)
            {
                frequency = omega * samplerate / OperationConsts.TwoPi;
                omega = omega + beta * error;

                phase = phase + omega + alpha * error;

                //phase wrap
                while (phase > OperationConsts.TwoPi)
                    phase -= OperationConsts.TwoPi;

                while (phase < -OperationConsts.TwoPi)
                    phase += OperationConsts.TwoPi;

                // mixer
                I = Math.Cos(phase) * samples_normalized[i].Real;
                Q = Math.Cos(phase) * samples_normalized[i].Imaginary;

                // LPFs 
                I = lpf1.ProcessSample(I);
                Q = lpf2.ProcessSample(Q);

                // VCO error
                error = I * Q;

                // loop filter
                error = lpf3.ProcessSample(error);

                //output
                result[i] = new Complex32((float)I, (float)Q);
            }

            InstantFrequency = frequency;

            //keep frequency in check
            if (double.IsNaN(frequency))
            {
                Reset();
            }

            return result;
        }
    }
}
