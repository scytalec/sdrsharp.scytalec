﻿/*
 * Paul Maxan, microp11 2021
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Costas Complex implemented as per
 * Best, Roland. Costas Loops Theory, Design, and Simulation. Springer International Publishing, 2018.
 * 
 * Not sensitive enough for symbol acquisition, but good enough for frequency error correction.
 * 
 */

using MathNet.Filtering.IIR;
using MathNet.Numerics;
using MathNet.Numerics.Statistics;
using System;

namespace SDRSharp.ScytaleC
{
    public class Synchronizer
    {
        // carrier frequency Hz
        private readonly double fC;
        private readonly double delta_f;

        //symbol rate Hz
        private readonly double fR;

        private readonly double fS;
        private readonly double T;
        private readonly double fT;
        private readonly double omegaT;
        private readonly double tau1;
        private readonly double K0;
        private readonly double omega0;
        private readonly double tR;
        private readonly double omegaC;
        private readonly double omegaT_P;

        //filters
        private readonly OnlineIirFilter LfFilter;
        private readonly OnlineIirFilter VcoFilter;

        private Complex32 mjphi2;

        public bool IsLocked { get; set; }
        public double Offset { get; internal set; }

        public Synchronizer()
        {
            fC = OperationConsts.CenterFrequency;
            delta_f = 0;
            fR = OperationConsts.SymbolRate;

            //sampling frequency of the(discrete) model
            fS = OperationConsts.SampleRate;

            //sampling interval of the model
            T = 1 / fS;

            //Transit frequency of PLL(Costas loop)
            fT = 0.4 * fR;

            omegaT = OperationConsts.TwoPi * fT;

            //integrator time constant in PI loop filter
            tau1 = 20e-6;

            //VCO gain
            K0 = tau1 * Math.Pow(omegaT, 2);

            omega0 = OperationConsts.TwoPi * (fC - delta_f);

            //duration of one symbol(bit)
            tR = 1 / fR;

            omegaC = OperationConsts.TwoPi * fC;

            //prewarp corner frequencies(for bilinear z transform)
            omegaT_P = (2 / T) * Math.Tan(omegaT * T / 2);

            //F3(z)
            //double[] F3_coefficients = new double[4] { 2 * tau1 / T, -2 * tau1 / T, 1 + 2 / (omegaT_P * T), 1 - 2 / (omegaT_P * T) };
            LfFilter = new OnlineIirFilter(OperationConsts.LfCoef48kHz);

            //F4(z)
            //double[] F4_coefficients = new double[4] { 1, -1, 0, T };
            VcoFilter = new OnlineIirFilter(OperationConsts.VcoCoef48kHz);

            mjphi2 = 0;
        }

        public void Reset()
        {
            LfFilter.Reset();
        }

        public Complex32[] ProcessSamples(Complex32[] samples)
        {
            int len = samples.Length;
            Complex32 sdem;
            float I;
            float Q;
            int Ib;
            Complex32 sdemIb;
            float ud;
            double uft;
            double uftx;
            double omega2;
            double phi2;
            Complex32[] result = new Complex32[len];
            double[] uftMedian = new double[len];

            Complex32[] samples_normalized = new Complex32[len];

            double magnitudeReal = 0.0;
            double meanMagnitudeReal;
            double magnitudeImag = 0.0;
            double meanMagnitudeImag;


            // sample normalization
            for (int i = 0; i < len; i++)
            {
                magnitudeReal += Math.Abs(samples[i].Real);
                magnitudeImag += Math.Abs(samples[i].Imaginary);

            }
            meanMagnitudeReal = magnitudeReal / len;
            meanMagnitudeImag = magnitudeImag / len;

            for (int i = 0; i < len; i++)
            {
                samples_normalized[i] = new Complex32((float)(samples[i].Real / meanMagnitudeReal), (float)(samples[i].Imaginary / meanMagnitudeImag));
            }

            for (int i = 0; i < len; i++)
            {
                sdem = samples_normalized[i] * mjphi2.Exponential();
                sdem = samples[i] * mjphi2.Exponential();
                I = sdem.Real;
                Q = sdem.Imaginary;
                result[i] = sdem;

                Ib = Math.Sign(I);
                sdemIb = sdem * Ib;
                ud = sdemIb.Phase;

                uft = LfFilter.ProcessSample(ud);
                uftMedian[i] = uft;

                uftx = uft * K0;
                omega2 = uftx + omega0;

                phi2 = VcoFilter.ProcessSample(omega2);

                mjphi2 = new Complex32(0, (float)(-1 * phi2));
            }

            Offset = uftMedian.Median();

            //keep offset in check
            if (Math.Abs(Offset) > 100)
            {
                LfFilter.Reset();
                VcoFilter.Reset();
                IsLocked = false;
            }
            else
            {
                IsLocked = true;
            }

            return result;
        }
    }
}

