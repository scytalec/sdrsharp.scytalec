﻿/*
 * Paul Maxan, microp11 2021
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using SDRSharp.Common;
using SDRSharp.Radio;
using SDRSharp.ScytaleC.Properties;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SDRSharp.ScytaleC
{
    public unsafe partial class DecoderPanel : UserControl
    {
        private readonly IQProcessor iQProcessor;
        private readonly ISharpControl control;
        private readonly SampleProcessor sampleProcessor;
        private bool processorIsStarted;
        private bool frequencyIsChangedAutomatically;
        private int tickCount;

        public DecoderPanel(IQProcessor iQProcessor, ISharpControl control)
        {
            InitializeComponent();

            this.iQProcessor = iQProcessor;
            this.control = control;
            this.control.PropertyChanged += new PropertyChangedEventHandler(PropertyChangedHandler);
            sampleProcessor = new SampleProcessor(this.iQProcessor, constellation1);
            txtUDPAddress.Text = Utils.GetStringSetting("ScytaleC.UDPAddress", "localhost");
            udUDPPort.Value = Utils.GetIntSetting("ScytaleC.UDPPort", 15003);
            tickCount = 0;
            lblVersion.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            EnableControls();
        }

        void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            // Events
            switch (e.PropertyName)
            {
                case "StartRadio":
                    EnableControls();
                    if (cbEnabled.Checked) StartDecoder();
                    break;

                case "StopRadio":
                    EnableControls();
                    StopDecoder();
                    break;

                case "Frequency":
                    if (frequencyIsChangedAutomatically)
                    {
                        frequencyIsChangedAutomatically = false;
                    }
                    else
                    {
                        sampleProcessor.SetCostasCenterFrequency();
                    }

                    break;

                default:
                    Debug.WriteLine("{0} property changed", e.PropertyName.ToString());
                    break;
            }
        }

        public void StoreSettings()
        {
            Utils.SaveSetting("ScytaleC.UDPAddress", txtUDPAddress.Text);
            Utils.SaveSetting("ScytaleC.UDPPort", (int)udUDPPort.Value);
        }

        private void EnableControls()
        {
            cbEnabled.Enabled = control.IsPlaying;
            udUDPPort.Enabled = !cbEnabled.Checked;
            txtUDPAddress.Enabled = !cbEnabled.Checked;
            if (!cbEnabled.Checked) lblLocked.Visible = false;
        }

        public void StartDecoder()
        {
            if (processorIsStarted) return;

            sampleProcessor.Start(txtUDPAddress.Text, (int)udUDPPort.Value);
            processorIsStarted = true;
            frequencyIsChangedAutomatically = false;

            EnableControls();
        }

        public void StopDecoder()
        {
            if (!processorIsStarted) return;

            sampleProcessor.Stop();
            processorIsStarted = false;
            frequencyIsChangedAutomatically = false;

            EnableControls();
        }
        int m = 0;
        private void DisplayTimer_Tick(object sender, EventArgs e)
        {
            if (sampleProcessor == null) return;
            if (!cbEnabled.Enabled) return;

            tickCount++;
            if (tickCount > 30)
            {
                tickCount = 0;
                lblLocked.Visible = sampleProcessor.IsLocked();
                txtOffset.Text = string.Format("{0:F2}", -sampleProcessor.Offset());
                txtFrequency.Text = string.Format("{0:F2}", sampleProcessor.Frequency());
                txtTx.Text = string.Format("{0}", sampleProcessor.TxFrameCount);
                txtLost.Text = string.Format("{0}", Math.Floor(sampleProcessor.EstimatedFrameCount) - sampleProcessor.TxFrameCount);
                txtFrameNumber.Text = string.Format("{0}", sampleProcessor.FrameNumber);
                txtSYM.Text = string.Format("{0:F3}", sampleProcessor.Syms / 1e6);
                if (sampleProcessor.LostBuffers > 0) lblLostBuffers.Text = string.Format("Lost Buffers: {0}", sampleProcessor.LostBuffers);
                lblDebug.Text = $"Est: {sampleProcessor.EstimatedFrameCount:F2}";
                if (lblDebug.Visible && cbEnabled.Checked)
                {
                    Logger.WriteStats($"{sampleProcessor.TxFrameCount},{sampleProcessor.EstimatedFrameCount:F2},{Math.Floor(sampleProcessor.EstimatedFrameCount) - sampleProcessor.TxFrameCount},{control.VisualSNR:F2},{sampleProcessor.FrameNumber}");
                }
                if (Math.Abs(OperationConsts.CenterFrequency - sampleProcessor.Frequency()) > 100) sampleProcessor.SetCostasCenterFrequency();
            }

            if (processorIsStarted && chkAutoTracking.Checked)
            {
                frequencyIsChangedAutomatically = true;
                if (Math.Abs(sampleProcessor.Offset()) > 0.7)
                {
                    m = 0;
                    control.Frequency += (int)(sampleProcessor.Offset());
                }
                else if (Math.Abs(sampleProcessor.Offset()) > 0.15)
                {
                    m++;
                    if (m > 70)
                    {
                        m = 0;
                        control.Frequency += Math.Sign(sampleProcessor.Offset());
                    }
                }
            }
        }

        private void CbEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (!control.IsPlaying) return;

            if (cbEnabled.Checked)
            {
                StartDecoder();
            }
            else if (!cbEnabled.Checked)
            {
                StopDecoder();
            }
        }

        private void LblTxLost_Click(object sender, EventArgs e)
        {
            if (((MouseEventArgs)e).Button == MouseButtons.Right)
            {
                lblDebug.Visible = !lblDebug.Visible;
            }
        }

        private void DecoderPanel_Load(object sender, EventArgs e)
        {
            chkAutoTracking.Checked = Settings.Default.chkAutoTracking;
            chkFrameRecorder.Checked = Settings.Default.chkFrameRecorder;
        }

        private void ChkAutoTracking_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.chkAutoTracking = chkAutoTracking.Checked;
            Settings.Default.Save();
        }

        private void ChkFrameRecorder_CheckedChanged(object sender, EventArgs e)
        {
            bool @checked = chkFrameRecorder.Checked;
            Settings.Default.chkFrameRecorder = @checked;
            sampleProcessor.IsRecording = @checked;
            Settings.Default.Save();
        }

        private void LblVersion_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                lblVersion.LinkVisited = true;
                Process.Start(new ProcessStartInfo("cmd", $"/c start https://bitbucket.org/scytalec/sdrsharp.scytalec/") { CreateNoWindow = true });
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        readonly string localQuickUIFolder = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\_scytalec-ui\";
        private void BtnOpenUI_Click(object sender, EventArgs e)
        {
            try
            {
                Directory.CreateDirectory(localQuickUIFolder);
                string url = $@"{localQuickUIFolder}ScytaleC.QuickUI.exe";
                //check if file exists
                string downloadUrl = @"https://bitbucket.org/scytalec/scytalec/downloads/";
                string uploadUrl = $@"{localQuickUIFolder}";
                if (!File.Exists(url))
                {
                    DialogResult dr = MessageBox.Show($"Scytalec.QuickUI file will be downloaded and extracted automatically from " +
                        $"'{downloadUrl}' " +
                        $"to " +
                        $"'{uploadUrl}'\n\n\n" +
                        $"Select YES if you agree or NO if you prefer doing it manually.", "QuickUI not found", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        try
                        {
                            DownloadAndInstall();
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show($"Cannot download QuickUI automatically: {ex.Message}");
                        }
                    }
                    return;
                }

                //check if process already running
                Process[] p = Process.GetProcessesByName("ScytaleC.QuickUI");
                if (p.Length == 0)
                {
                    Process.Start(new ProcessStartInfo { FileName = url, UseShellExecute = true });
                }
                else
                {
                    //bring to front
                    IntPtr handle = p[0].MainWindowHandle;
                    SetForegroundWindow(handle);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void DownloadAndInstall()
        {
            using WebClient wc = new WebClient();
            wc.DownloadProgressChanged += Wc_DownloadProgressChanged;
            wc.DownloadFileCompleted += Wc_DownloadFileCompleted;
            wc.DownloadFileAsync(
                // Param1 = Link of file
                new Uri("https://bitbucket.org/scytalec/scytalec/downloads/x64-ScytaleC.QuickUI-17010.zip"),
                // Param2 = Path to save
                $@"{localQuickUIFolder}x64-ScytaleC.QuickUI-17010.zip"
            );
            lblLostBuffers.Text = "";
            lblLostBuffers.Visible = true;
        }

        private void Wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
            //System.IO.DirectoryInfo di = new DirectoryInfo(localQuickUIFolder); 
            //foreach (FileInfo file in di.EnumerateFiles())
            //{
            //    file.Delete();
            //}
            ZipFile.ExtractToDirectory($@"{localQuickUIFolder}x64-ScytaleC.QuickUI-17010.zip", $@"{localQuickUIFolder}", true);
            lblLostBuffers.Visible = false;
            lblLostBuffers.Text = "";
            MessageBox.Show($"QuickUI has been successfully installed");
            BtnOpenUI_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Quick UI Error");
            }
        }

        void Wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            lblLostBuffers.Text = $"{e.ProgressPercentage} %";
        }
    }
}
