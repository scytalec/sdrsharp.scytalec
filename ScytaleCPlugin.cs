﻿/*
 * Paul Maxan, microp11 2021
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using SDRSharp.Common;
using SDRSharp.Radio;
using System;
using System.Windows.Forms;

namespace SDRSharp.ScytaleC
{
    public class ScytaleCPlugin : ISharpPlugin, IDisposable
    {
        private const string displayName = "Scytale-C";

        private ISharpControl control;
        private IQProcessor iQProcessor;
        private DecoderPanel uiControl;

        public string DisplayName
        {
            get { return displayName; }
        }

        public UserControl Gui
        {
            get { return uiControl; }
        }

        public void Initialize(ISharpControl control)
        {
            this.control = control;
            iQProcessor = new IQProcessor();
            this.control.RegisterStreamHook(iQProcessor, ProcessorType.DecimatedAndFilteredIQ);
            uiControl = new DecoderPanel(iQProcessor, this.control);
        }

        public void Close()
        {
            uiControl.StoreSettings();
            uiControl.StopDecoder();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    /// <summary>
    /// Computed as described:
    /// http://thinair.aelogic.com/transfer-function-to-c-filter-coefficients-breakthrough/
    /// </summary>
    public static class OperationConsts
    {
        public const double TwoPi = 2 * Math.PI;

        //stream params
        public const double SymbolRate = 1200.0;
        public const double CenterFrequency = 2000.0;
        public const double FrameDuration = 24 * 60 * 60 / 10000.0;

        //48000
        public const int SampleRateInt = 48000;
        public const double SampleRate = SampleRateInt * 1.0;
        public static double[] LfCoef48kHz = new double[6] { 17.0940187259239, -16.0523520592573, 0, 1, -1, 0 };
        public static double[] VcoCoef48kHz = new double[6] { 0, 2.08333333333333e-05, 0, 1, -1, 0 };
    }
}
