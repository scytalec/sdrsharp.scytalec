﻿/*
 * Paul Maxan, microp11 2021
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * https://www.techotopia.com/index.php/Using_Bitmaps_for_Persistent_Graphics_in_C_Sharp
 */

using MathNet.Numerics;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace SDRSharp.ScytaleC
{
    public partial class Constellation : UserControl
    {
        private Bitmap image;
        private Point origin;
        private double scalingFactor = 17.0;

        public BufferBlock<Complex32[]> SymbolSignal;

        public Constellation()
        {
            InitializeComponent();

            SymbolSignal = new BufferBlock<Complex32[]>();

            origin = new Point(ClientRectangle.Width / 2, ClientRectangle.Height / 2);
            image = new Bitmap(ClientRectangle.Width, ClientRectangle.Height, PixelFormat.Format24bppRgb);
            using Graphics surface = Graphics.FromImage(image);
            surface.Clear(Color.Black);
        }

        public void Start()
        {
            timer1.Enabled = true;
        }

        public void Stop()
        {
            timer1.Enabled = false;
        }

        private void DisposeExtraResources()
        {
            image.Dispose();
        }

        private void Graph_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImageUnscaled(image, 0, 0);
        }

        private void Graph_Resize(object sender, EventArgs e)
        {
            try
            {
                origin = new Point(ClientRectangle.Width / 2, ClientRectangle.Height / 2);
                if (image != null)
                {
                    image.Dispose();
                }
                image = new Bitmap(ClientRectangle.Width, ClientRectangle.Height, PixelFormat.Format24bppRgb);
            }
            catch { }
            Debug.WriteLine($"{ClientRectangle.Width}");
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (SymbolSignal.Count == 0) return;

                Complex32[] chunk = SymbolSignal.Receive();
                int len = chunk.Length;

                using (Graphics surface = Graphics.FromImage(image))
                {
                    surface.Clear(Color.Black);
                }

                for (int i = 0; i < len; i++)
                {
                    Point point = new Point(
                        origin.X + (int)(chunk[i].Real * ClientRectangle.Width / scalingFactor),
                        origin.Y + (int)(chunk[i].Imaginary * ClientRectangle.Height / scalingFactor));

                    if (ClientRectangle.Contains(point))
                    {
                        image.SetPixel(point.X, point.Y, Color.Yellow);
                    }
                }

                Invalidate(ClientRectangle);
            }
            catch { }
        }
    }
}
