﻿/*
 * Paul Maxan, microp11 2021
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace SDRSharp.ScytaleC
{
    public class LogUtils
    {
        private static readonly char[] hexChars = new char[] {
            '0', '1', '2', '3',
            '4', '5', '6', '7',
            '8', '9', 'A', 'B',
            'C', 'D', 'E', 'F'
        };

        /// <summary>
        /// Return the hex representation of the input array.
        /// </summary>
        /// <param name="input">Input byte array.</param>
        /// <param name="start">Starts converting bytes to hex from "start", inclusive.</param>
        /// <param name="length">Converts "length" bytes.</param>
        /// <returns>Returns the exception message if any error occurs.</returns>
        public static string BytesToHexString(byte[] input, int start, int length)
        {
            try
            {
                StringBuilder result = new StringBuilder();
                for (int i = start; i < start + length; i++)
                {
                    result.Append(hexChars[(byte)(input[i] & 0xF0) >> 4]);
                    result.Append(hexChars[input[i] & 0x0F]);
                }

                return result.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }

    public static class Logger
    {
        private static readonly string root;

        static Logger()
        {
            root = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\_data";
        }

        static public void WriteFrame(string msg)
        {
            try
            {
                DirectoryInfo di = Directory.CreateDirectory(root);
                string f = $@"{di.FullName}\frames-{DateTime.UtcNow:yyyy-MM-dd}.log";
                using StreamWriter sw = new StreamWriter(f, true);
                sw.WriteLine(msg);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"{msg} : {ex.Message}");
            }
        }

        static public void WriteError(string msg)
        {
            try
            {
                DirectoryInfo di = Directory.CreateDirectory(root);
                string f = $@"{di.FullName}\error-{DateTime.UtcNow:yyyy-MM-dd}.log";
                using StreamWriter sw = new StreamWriter(f, true);
                sw.WriteLine(msg);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"{msg} : {ex.Message}");
            }
        }

        internal static void WriteStats(string msg)
        {
            try
            {
                DirectoryInfo di = Directory.CreateDirectory(root);
                string f = $@"{di.FullName}\stats-{DateTime.UtcNow:yyyy-MM-dd}.log";
                using StreamWriter sw = new StreamWriter(f, true);
                sw.WriteLine(msg);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"{msg} : {ex.Message}");
            }
        }

        internal static void WriteInput(string msg)
        {

        }

        internal static void WriteOutput(string msg)
        {

        }
    }

}
