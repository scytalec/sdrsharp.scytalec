﻿/*
 * Paul Maxan, microp11 2021
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * https://stackoverflow.com/questions/461742/how-to-convert-an-ipv4-address-into-a-integer-in-c
 * 
 * 
 */

using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading.Tasks.Dataflow;

namespace ScytaleC.Decoder
{
    public class TcpRx
    {
        private const int samplesPerRead = 8192;

        private BufferBlock<byte[]> buffer;
        private NetworkStream tcpStream;
        private TcpClient tcpClient;
        private bool continueToReadTcp;
        private byte[] tcpData;

        public event EventHandler<string> OnFeedback;
        public event EventHandler<string> OnDisconnect;

        public TcpRx()
        {
            tcpStream = null;
            tcpClient = null;
        }

        public void Disconnect()
        {
            continueToReadTcp = false;
        }

        public void Connect(string inAddress, int inPort, BufferBlock<byte[]> buffer)
        {
            try
            {
                if (tcpStream != null)
                {
                    tcpStream.Dispose();
                    tcpStream = null;
                }

                if (tcpClient != null)
                {
                    tcpClient.Close();
                    tcpClient = null;
                }

                if (this.buffer != null)
                {
                    this.buffer = null;
                }

                this.buffer = buffer;

                tcpClient = new TcpClient(inAddress, inPort);
                continueToReadTcp = true;

                tcpStream = tcpClient.GetStream();
                tcpData = new byte[samplesPerRead];
                tcpStream.BeginRead(tcpData, 0, samplesPerRead, OnTcpReadAsyncComplete, null);
                OnFeedback?.Invoke(this, "Receiving data...");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                OnDisconnect?.Invoke(this, ex.Message);
            }
        }

        private void OnTcpReadAsyncComplete(IAsyncResult ar)
        {
            int bytesRead;
            try
            {
                bytesRead = tcpStream.EndRead(ar);
                byte[] data = new byte[bytesRead];
                Buffer.BlockCopy(tcpData, 0, data, 0, bytesRead);

                //tcpip in
                buffer.Post(data);
                //Debug.WriteLine("bytes read {0}", bytesRead);
            }
            catch
            {
                bytesRead = 0;
            }

            if (!continueToReadTcp || bytesRead == 0)
            {
                if (bytesRead == 0)
                {
                    //eof reached
                    OnDisconnect?.Invoke(this, "End of stream reached.");
                }
                else
                {
                    //the user has pressed the stop button
                    OnDisconnect?.Invoke(this, "The user has pressed the Stop button.");
                }
                Debug.WriteLine(bytesRead.ToString());
            }
            else
            {
                tcpData = new byte[samplesPerRead];
                tcpStream.BeginRead(tcpData, 0, samplesPerRead, OnTcpReadAsyncComplete, null);
            }
        }
    }
}
