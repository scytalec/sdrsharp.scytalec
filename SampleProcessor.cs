﻿/*
 * Paul Maxan, microp11 2021
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Most of the code below was authored by Vasili, who kindly allowed me to use and modify
 * the code of his TcpServer plugin:
 * http://thinair.aelogic.com/iq-out/
 * 
 */

using MathNet.Numerics;
using ScytaleC.Decoder;
using SDRSharp.Radio;
using System;
using System.Threading;
using System.Threading.Tasks.Dataflow;

namespace SDRSharp.ScytaleC
{
    public unsafe class SampleProcessor : IDisposable
    {
        private const int SamplesInPacket = 512 * 32;

        //for AGC in seconds
        private const float TimeConst = 0.01f;

        //Output samplerate fixed
        private const int outputSamplerate = OperationConsts.SampleRateInt;

        private readonly ComplexFifoStream iQStream;
        private readonly IQProcessor iQProcessor;
        private readonly float alpha;
        private readonly float beta;

        private Resampler resamplerI, resamplerQ;
        private readonly UnsafeBuffer complexResamplerBuffer;
        private readonly Complex* complexResamplerBufferPtr;

        private bool iQEnabled;
        private int maxBufferSize;
        private double iQSampleRate;
        private byte[] demodulatedBuffer;

        private UnsafeBuffer samplesBuffer;
        private Complex* samplesBufferPtr;
        private Thread iQHandlerThread;
        private Thread udpHandlerThread;

        private float dataI = 0.0f;
        private float gainI = 0.0f;
        private float maxI = 0.0f;

        private float dataQ = 0.0f;
        private float gainQ = 0.0f;
        private float maxQ = 0.0f;

        public int LostBuffers { get; private set; }

        public int BufferSize { get; private set; }
        public int TxFrameCount { get; private set; }
        public double EstimatedFrameCount { get; private set; }
        public long Syms { get; private set; }
        public bool IsRecording { get; set; }

        private readonly Synchronizer synchronizer;
        private readonly Costas costas;
        private readonly Rrc rrc;
        private readonly Gardner gardner;

        private ScytaleCDecoder decoder;

        //demodulator input
        private BufferBlock<byte[]> inputDemodulated;

        //udp
        private BufferBlock<byte[]> outputDecoded;
        private string udpAddress;
        private int udpPort;
        private UdpTx udpOutput;

        private readonly long StreamId;
        private DateTime timeOrigin;
        public int FrameNumber;

        private readonly Constellation constellation;

        public void SetCostasCenterFrequency(int value = 2000)
        {
            if (costas != null)
            {
                costas.SetCenterFrequency(value);
            }
        }

        /// <summary>
        /// Constructor for SampleProcessor. Taps into the IQProcessor and attaches a callback
        /// to receive the IQ data.
        /// </summary>
        /// <param name="iQProcessor"></param>
        public SampleProcessor(IQProcessor iQProcessor, Constellation constellation)
        {
            this.iQProcessor = iQProcessor;
            this.iQProcessor.IQReady += IQProcessor_IQReady;
            this.iQProcessor.Enabled = false;
            this.constellation = constellation;

            alpha = (float)(1.0 - Math.Exp(-1.0f / (outputSamplerate * TimeConst)));
            beta = 1 - alpha;

            iQStream = new ComplexFifoStream(BlockMode.None, outputSamplerate);

            complexResamplerBuffer = UnsafeBuffer.Create(outputSamplerate, sizeof(Complex));
            complexResamplerBufferPtr = (Complex*)complexResamplerBuffer;

            //demodulation
            synchronizer = new Synchronizer();
            costas = new Costas();
            rrc = new Rrc(OperationConsts.SampleRate, OperationConsts.SymbolRate);
            gardner = new Gardner(OperationConsts.SampleRate, OperationConsts.SymbolRate);

            DateTime now = DateTime.UtcNow;
            StreamId = (now.Ticks / 10000);
            string sl = StreamId.ToString();
        }

        public double Frequency()
        {
            return costas.InstantFrequency;
        }

        public double Offset()
        {
            return synchronizer.Offset;
        }

        public bool IsLocked()
        {
            return gardner.Locked;
        }

        /// <summary>
        /// Callback providing the IQ samples. The output is input resampled to given fixed sample
        /// rate and corrected for magnitude. The data received is DecimatedAndFilteredIQ.
        /// 
        /// If you implement the below for larger bandwidths, use the ComplexDecimator wrapper and 
        /// decimate by 2^N, where N is given by the formula:
        /// double res = Math.Log(deviceIQSR / 48000.0, 2);
        /// int N = (int)Math.Floor((decimal)res);
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="deviceIQSR"></param>
        /// <param name="length"></param>
        private void IQProcessor_IQReady(Complex* buffer, double deviceIQSR, int length)
        {
            if (iQStream == null) return;

            try
            {
                if (deviceIQSR != iQSampleRate)
                {
                    if (deviceIQSR <= 0 || deviceIQSR > 400000) return;

                    //this is to get rid of an out of memory exception on an odd device sample rate as
                    //RTL_SDR comes out with an extra 1KHz for a particular device sampling
                    //and this generates ridiculous numbers withing the resampler
                    int diqsr = (int)(deviceIQSR);

                    //we should not sample directly from device to audio rate, decimation should be emplyed
                    //to save CPU. For the USB bandwidth we're using though, decimation is not needed as
                    //deviceIQSR is already very close to 48KHz
                    maxBufferSize = outputSamplerate;
                    resamplerI = new Resampler(diqsr, outputSamplerate);
                    resamplerQ = new Resampler(diqsr, outputSamplerate);

                    iQSampleRate = deviceIQSR;
                }

                float* in_ptr = (float*)buffer;
                float* out_ptr = (float*)complexResamplerBuffer;

                var iQLength = resamplerI.ProcessInterleaved(in_ptr, out_ptr, length);
                resamplerQ.ProcessInterleaved(in_ptr + 1, out_ptr + 1, length);

                if (iQStream.Length > maxBufferSize - iQLength)
                {
                    LostBuffers++;
                }
                else
                {
                    BufferGain(complexResamplerBufferPtr, iQLength);
                    iQStream.Write(complexResamplerBufferPtr, iQLength);
                }

                BufferSize = (int)((iQStream.Length / (float)maxBufferSize) * 100.0f);
            }
            catch (Exception ex)
            {
                Logger.WriteError($"deviceIQSR {deviceIQSR}, length {length}, msg {ex.Message}");
            }
        }

        public void Start(string UdpAddress, int UdpPort)
        {
            constellation.Start();

            //repeater
            udpAddress = UdpAddress;
            udpPort = UdpPort;

            iQEnabled = true;

            LostBuffers = 0;

            iQHandlerThread = new Thread(IQHandlerThread);
            iQHandlerThread.Start();

            iQProcessor.Enabled = true;

            synchronizer.Reset();
            costas.Reset();
            rrc.Reset();

            //scytalec decoder
            inputDemodulated = new BufferBlock<byte[]>();
            outputDecoded = new BufferBlock<byte[]>();

            //create decoder and start the decoding of input symbols. Continous, async.
            decoder = new ScytaleCDecoder(9, inputDemodulated, outputDecoded);
            decoder.OnDemodulatedSymbols -= Decoder_OnDemodulatedSymbols;
            decoder.OnDemodulatedSymbols += Decoder_OnDemodulatedSymbols;

            //Set UDP
            udpOutput = new UdpTx();
            udpHandlerThread = new Thread(UDPHandlerThread);
            udpHandlerThread.Start();

            Syms = 0;
            TxFrameCount = 0;
        }

        /// <summary>
        /// UDP handler in its own thread
        /// </summary>
        private void UDPHandlerThread()
        {
            while (iQEnabled)
            {
                if (outputDecoded.Count == 0)
                {
                    EstimateLostFrames(DateTime.Now);
                    Thread.Sleep(1000);
                    continue;
                }

                try
                {

                    byte[] chunk = outputDecoded.Receive();

                    FrameNumber = chunk[2] << 8 | chunk[3];

                    //Append the StreamId to the frame
                    byte[] datagram = new byte[chunk.Length + 8];
                    Array.Copy(chunk, datagram, chunk.Length);
                    byte[] sid = BitConverter.GetBytes(StreamId);
                    Array.Copy(sid, 0, datagram, chunk.Length, 8);

                    //log data
                    if (IsRecording)
                    {
                        Logger.WriteFrame($"{FrameNumber:D4} {StreamId:D16} {LogUtils.BytesToHexString(chunk, 0, chunk.Length)}");
                        //LogFrames.Debug("{0:D4} {1:D16} {2}", FrameNumber, StreamId, LogUtils.BytesToHexString(chunk, 0, chunk.Length));
                    }

                    //send data out over udp
                    TxFrameCount++;
                    EstimateLostFrames(DateTime.Now);
                    udpOutput.Send(udpAddress, udpPort, datagram);
                }
                catch (Exception ex)
                {
                    Logger.WriteError(ex.Message);
                }
            }
        }

        private void EstimateLostFrames(DateTime time)
        {
            if (TxFrameCount < 1)
            {

            }
            else if (TxFrameCount == 1)
            {
                timeOrigin = time;
                EstimatedFrameCount = 1;
            }
            else
            {
                TimeSpan span = time - timeOrigin;
                double estimatedFrames = span.TotalSeconds / OperationConsts.FrameDuration;
                EstimatedFrameCount = 2 + estimatedFrames;
            }
        }

        private void Decoder_OnDemodulatedSymbols(object sender, DemodulatedSymbolsArgs e)
        {
            Syms += e.Length;
        }

        /// <summary>
        /// Stops the decoder
        /// </summary>
        public void Stop()
        {
            iQEnabled = false;
            iQProcessor.Enabled = false;

            if (resamplerI != null)
            {
                resamplerI = null;
            }

            if (resamplerQ != null)
            {
                resamplerQ = null;
            }

            if (iQHandlerThread != null)
            {
                iQHandlerThread.Join();
                iQHandlerThread = null;
            }

            if (udpHandlerThread != null)
            {
                udpHandlerThread.Join();
                udpHandlerThread = null;
            }

            iQSampleRate = 0;
            BufferSize = 0;

            inputDemodulated.Complete();
            outputDecoded.Complete();

            constellation.Stop();
        }

        /// <summary>
        /// IQ stream handler in its own thread
        /// </summary>
        private void IQHandlerThread()
        {
            while (iQEnabled)
            {
                if (iQStream.Length < SamplesInPacket)
                {
                    Thread.Sleep(10);
                    continue;
                }

                if (samplesBuffer == null)
                {
                    //create buffer
                    samplesBuffer = UnsafeBuffer.Create(SamplesInPacket, sizeof(Complex));
                    samplesBufferPtr = (Complex*)samplesBuffer;
                }

                //fill buffer
                iQStream.Read(samplesBufferPtr, SamplesInPacket);

                //Costas
                Complex32[] samples = new Complex32[SamplesInPacket];
                for (int i = 0; i < SamplesInPacket; i++)
                {
                    samples[i] = new Complex32(samplesBufferPtr[i].Real, samplesBufferPtr[i].Imag);

                    //log input
                    Logger.WriteInput("{samplesBufferPtr[i].Real},{samplesBufferPtr[i].Imag}");
                }

                //works only with higher SNR
                //used as frequency corrector
                synchronizer.ProcessSamples(samples);

                samples = costas.ProcessSamples(samples);

                //RRC
                samples = rrc.ProcessSamples(samples);

                //Gardner
                Complex32[] outputGardner = gardner.ProcessSamples(samples).ToArray();

                //output constellation
                constellation.SymbolSignal.Post(outputGardner);

                //looping 
                for (int i = 0; i < outputGardner.Length; i++)
                {
                    Logger.WriteOutput("{outputGardner[i].Real},{outputGardner[i].Imaginary}");
                }

                //output buffer of variable length
                demodulatedBuffer = null;
                demodulatedBuffer = new byte[outputGardner.Length];
                CreateDemodulatedPacket(outputGardner, demodulatedBuffer);

                //decode symbols into packets
                if (inputDemodulated != null) inputDemodulated.Post(demodulatedBuffer);
            }
        }

        /// <summary>
        /// Amplifies in-place the IQ samples in buffer to values between [-1 1]
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="length"></param>
        private void BufferGain(Complex* buffer, int length)
        {
            for (int i = 0; i < length; i++)
            {
                //I
                dataI = buffer[i].Real;
                dataI = dataI >= 0 ? dataI : -dataI;

                maxI *= beta;
                maxI = maxI > dataI ? maxI : dataI;

                if (maxI > 0)
                {
                    gainI = gainI * beta + (0.6f / maxI) * alpha;
                }
                buffer[i].Real *= gainI;

                //Q
                dataQ = buffer[i].Imag;
                dataQ = dataQ >= 0 ? dataQ : -dataQ;

                maxQ *= beta;
                maxQ = maxQ > dataQ ? maxQ : dataQ;

                if (maxQ > 0)
                {
                    gainQ = gainQ * beta + (0.6f / maxQ) * alpha;
                }
                buffer[i].Imag *= gainQ;
            }
        }

        private static void CreateDemodulatedPacket(Complex32[] source, byte[] dest)
        {
            if (source == null || dest == null) return;
            int destIndex = 0;

            for (int i = 0; i < source.Length; i++)
            {
                int value = (Math.Sign(source[i].Real) + 1) / 2;
                dest[destIndex++] = (byte)(value);
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
